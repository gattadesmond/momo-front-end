"use strict";

var htmlElement = document.documentElement;
var bodyElement = document.body;
var pageOverlay = document.getElementById("overlay");

var Layout = (function() {
  var setUserAgent = function setUserAgent() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      bodyElement.classList.add("window-phone");
      return;
    }

    if (/android/i.test(userAgent)) {
      bodyElement.classList.add("android");
      return;
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      bodyElement.classList.add("ios");
      return;
    }
  };

  var scrollToTop = function scrollToTop() {
    var scrollTop = $("#back-to-top ");

    $(window).on("scroll", function() {
      if ($(window).scrollTop() > 300) {
        scrollTop.addClass("active");
      } else {
        scrollTop.removeClass("active");
      }
    });

    scrollTop.find(".top").click(function() {
      $("body, html").animate(
        {
          scrollTop: 0
        },
        500
      );
    });
  };

  var handleMenuAim = function handleMenuAim() {
    $("#juno-menu-aim").menuAim({
      activate: function activate(row) {
        $(row)
          .children()
          .addClass("is-active");
      },
      deactivate: function deactivate(row) {
        $(row)
          .children()
          .removeClass("is-active");
      },
      exitMenu: function exitMenu() {
        $(".cd-dropdown-content")
          .find(".is-active")
          .removeClass("is-active");
        return true;
      },
      submenuDirection: "right"
    });
  };

  var handleSticky = function() {
    $(".sticky-content, .sticky-sidebar").theiaStickySidebar({
      // Settings
      additionalMarginTop: 70
    });
  };

  var linkScroll = function() {
    $("a[data-scrollto]").click(function() {
      var element = $(this),
        divScrollToAnchor = element.attr("data-scrollto"),
        divScrollSpeed = element.attr("data-speed"),
        divScrollOffset = element.attr("data-offset");

      // if (element.parents("#primary-menu").hasClass("on-click")) {
      //   return true;
      // }

      if (!divScrollSpeed) {
        divScrollSpeed = 750;
      }
      if (!divScrollOffset) {
        divScrollOffset = 0;
      }

      $("html,body")
        .stop(true)
        .animate(
          {
            scrollTop:
              $(divScrollToAnchor).offset().top - Number(divScrollOffset)
          },
          Number(divScrollSpeed)
        );

      return false;
    });
  };

  return {
    init: function init() {
      setUserAgent();
      handleMenuAim();
      scrollToTop();
      handleSticky();
      linkScroll();
    }
  };
})();

//set scrolling variables
var scrolling = false,
  previousTop = 0,
  currentTop = 0,
  scrollDelta = 10,
  scrollOffset = 100,
  scrollNav = 200,
  scrollPromotion = 600;

function autoHideHeader() {
  var currentTop = $(window).scrollTop();

  if (currentTop >= scrollNav) {
    bodyElement.classList.add("is-scroll-nav");
  } else {
    bodyElement.classList.remove("is-scroll-nav");
  }

  if (currentTop >= scrollPromotion) {
    bodyElement.classList.add("is-scroll-promotion");
  } else {
    bodyElement.classList.remove("is-scroll-promotion");
  }

  if (previousTop - currentTop > scrollDelta) {
    //if scrolling up...
    bodyElement.classList.remove("is-scroll-down");
  } else if (
    currentTop - previousTop > scrollDelta &&
    currentTop > scrollOffset
  ) {
    //if scrolling down...
    bodyElement.classList.add("is-scroll-down");
  }

  previousTop = currentTop;
  scrolling = false;
}

autoHideHeader();

$(".bottom-promotion-close").on("click", function(e) {
  e.preventDefault();
  $(".bottom-promotion").hide();
});

$(".bottom-promotion2-content").on("click", function(e) {
  e.preventDefault();

  $(".bottom-promotion2").addClass("is-active");
});

$(".bottom-promotion-close2").on("click", function(e) {
  e.preventDefault();
  $(".bottom-promotion2").removeClass("is-active");
});

$(window).on("scroll", function() {
  if (!scrolling) {
    scrolling = true;
    !window.requestAnimationFrame
      ? setTimeout(autoHideHeader, 250)
      : requestAnimationFrame(autoHideHeader);
  }
});

function centerItFixedWidth(target, outer) {
  var out = $(outer);
  var tar = $(target);

  var x = out.outerWidth(true);
  var y = tar.outerWidth(true);

  var z = tar.offset().left + y / 2 + out.scrollLeft() - x / 2;

  out.animate({
    scrollLeft: z
  });
}

$(document).ready(function() {
  Layout.init();

  $(".kv-svg").rating({
    theme: "krajee-svg",
    filledStar: '<span class="krajee-icon krajee-icon-star"></span>',
    emptyStar: '<span class="krajee-icon krajee-icon-star"></span>',
    showCaption: false,
    starCaptions: {
      0: "Not Rated",
      1: "Very Poor",
      2: "Poor",
      3: "Ok",
      4: "Good",
      5: "Very Good"
    },
    starCaptionClasses: {
      0: "text-danger",
      1: "text-danger",
      2: "text-warning",
      3: "text-info",
      4: "text-primary",
      5: "text-success"
    }
  });

  $('[data-toggle="tooltip"]').tooltip();

  $(".popup-youtube, .popup-vimeo, .popup-gmaps").magnificPopup({
    disableOn: 700,
    type: "iframe",
    mainClass: "mfp-fade",
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });

  var breakpoint = {};
  breakpoint.refreshValue = function() {
    this.value = window
      .getComputedStyle(document.querySelector("body"), ":before")
      .getPropertyValue("content")
      .replace(/\"/g, "");
  };

  $(window)
    .resize(function() {
      breakpoint.refreshValue();
    })
    .resize();

  if (breakpoint.value == "desktop") {
    var galleryTop = new Swiper(".gallery-top", {
      spaceBetween: 0,
      slidesPerView: 1,
      lazy: {
        loadPrevNext: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      pagination: {
        el: ".swiper-pagination"
      },
      on: {
        slideChangeTransitionEnd: function slideChangeTransitionEnd() {
          var num = galleryTop.activeIndex;
          $(".gallery-thumbs .is-active").removeClass("is-active");
          $(".gallery-thumbs .swiper-slide")
            .eq(num)
            .addClass("is-active");

          if (this.previousIndex < this.activeIndex) {
            galleryThumbs.slideTo(this.activeIndex - 3);
          } else {
            galleryThumbs.slideTo(this.activeIndex - 1);
          }
        }
      }
    });

    var galleryThumbs = new Swiper(".gallery-thumbs", {
      spaceBetween: 0,
      slidesPerView: "auto"
    });

    $(".gallery-thumbs .swiper-slide").each(function(index, element) {
      var num = index;
      $(element).on("click", function(e) {
        e.preventDefault();
        galleryTop.slideTo(index);
      });
    });

    var swiperProduct = new Swiper(".product-slider", {
      slidesPerView: 5,
      slidesPerGroup: 1,
      spaceBetween: 0,
      breakpoints: {
        767: {
          slidesPerView: 3
        },
        540: {
          slidesPerView: 2
        },
        320: {
          slidesPerView: 1
        }
      },
      // Enable lazy loading
      lazy: {
        loadPrevNext: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    });
  }

  var swiperPost = new Swiper(".post-slider", {
    // Disable preloading of all images
    preloadImages: false,
    watchOverflow: true,
    loop: true,
    // Enable lazy loading
    lazy: {
      loadPrevNext: true
    },
    slidesPerView: 1,
    speed: 800,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: ".swiper-pagination"
      // clickable: true,
    }
  });

  var galleryMobile = new Swiper(".gallery-mobile", {
    spaceBetween: 0,
    slidesPerView: 1,
    lazy: {
      loadPrevNext: true
    },
    pagination: {
      el: ".swiper-pagination"
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  });

  var dttSlider = new Swiper(".dtt-slider", {
    // Disable preloading of all images
    preloadImages: false,
    watchOverflow: true,
    loop: true,
    // Enable lazy loading
    lazy: {
      loadPrevNext: true
    },
    slidesPerView: "auto",
    speed: 800,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: ".swiper-pagination"
      // clickable: true,
    }
  });

  $(".manual-device").each(function(index, element) {
    var $this = $(this);
    var numSlider = $this.attr("childId");
    var device = null;
    var process = null;

    if (numSlider != undefined) {
      device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
      process = $this
        .parent()
        .parent()
        .find(".manual-process#hd-sub-ctn-" + numSlider);
    } else {
      numSlider = index;
      device = $this.find(".manual-device-swiper");

      process = $this
        .parent()
        .parent()
        .find(".manual-process ");
    }

    var swiperArray = [];

    swiperArray[numSlider] = new Swiper(device, {
      observer: true,
      observeParents: true,
      on: {
        slideChangeTransitionEnd: function slideChangeTransitionEnd() {
          var num = this.activeIndex;
          process.children(".process_item").removeClass("active");
          process
            .children(".process_item")
            .eq(num)
            .addClass("active");
        }
      }
    });

    process.children(".process_item").each(function(index, element) {
      var num = index;
      $(element).on("click", function(e) {
        e.preventDefault();
        $(this)
          .addClass("active")
          .siblings()
          .removeClass("active");
        swiperArray[numSlider].slideTo(index);
      });
    });
  });

  $(".process__body-content").each(function(index, element) {
    var $this = $(this);
    var height = $this[0].scrollHeight;
    var getHeight = height == "0" ? "auto" : height + "px";
    $this.css("--max-height", getHeight);
  });

  $(".guide-desktop-slider").each(function(index, element) {
    var $this = $(this);
    var numSlider = $this.attr("childId");
    var device = null;
    var process = null;

    if (numSlider != undefined) {
      device = $this.find(".guide-device-swiper#hd-sub-swp-" + numSlider);
      process = $this
        .parent()
        .parent()
        .find(".guide-step#hd-sub-ctn-" + numSlider);
    } else {
      numSlider = index;
      device = $this.find(".guide-device-swiper");

      process = $this
        .parent()
        .parent()
        .find(".guide-step");
    }

    var swiperArray = [];

    swiperArray[numSlider] = new Swiper(device, {
      observer: true,
      observeParents: true,

      effect: "coverflow",
      // loop: true,
      centeredSlides: true,
      grabCursor: true,
      keyboardControl: true,
      mousewheelControl: true,
      preventClicks: false,
      preventClicksPropagation: false,
      spaceBetween: 30,
      slidesPerView: "auto",
      coverflowEffect: {
        rotate: 5,
        stretch: 0,
        depth: 100,
        modifier: 3,
        slideShadows: false
      },

      on: {
        slideChangeTransitionEnd: function slideChangeTransitionEnd() {
          var num = this.activeIndex;
          process.children(".guide-step-item").removeClass("active");
          process
            .children(".guide-step-item")
            .eq(num)
            .addClass("active");
        }
      }
    });

    process.children(".guide-step-item").each(function(index, element) {
      var num = index;
      $(element).on("click", function(e) {
        e.preventDefault();
        $(this)
          .addClass("active")
          .siblings()
          .removeClass("active");
        swiperArray[numSlider].slideTo(index);
      });
    });
  });

  var D = $(".countdown-s2");
  D.length > 0 &&
    D.each(function() {
      var e = $(this),
        i = e.attr("data-date");
      e.countdown(i).on("update.countdown", function(e) {
        $(this).html(
          e.strftime(
            '<div class="countdown-s2-item"><span class="countdown-s2-time countdown-time-first">%D</span><span class="countdown-s2-text">NGÀY</span></div><div class="countdown-s2-item"><span class="countdown-s2-time">%H</span><span class="countdown-s2-text">GIỜ</span></div><div class="countdown-s2-item"><span class="countdown-s2-time">%M</span><span class="countdown-s2-text">PHÚT</span></div><div class="countdown-s2-item"><span class="countdown-s2-time countdown-time-last">%S</span><span class="countdown-s2-text">GIÂY</span></div>'
          )
        );
      });
    });

  $(".manual-page-tab .nav-item").on("shown.bs.tab", function(e) {
    var nodesArray = Array.prototype.slice.call(
      document.querySelectorAll(".manual-device-swiper")
    );
    nodesArray.forEach(function(e) {
      e.swiper.update();
    });

    centerItFixedWidth(".manual-page-tab .nav-item.active", ".manual-page-tab");
  });

  $(".desnav .nav-item").on("shown.bs.tab", function(e) {
    var nodesArray = Array.prototype.slice.call(
      document.querySelectorAll(".manual-device-swiper")
    );
    nodesArray.forEach(function(e) {
      e.swiper.update();
    });

  });


  var promoSlider = new Swiper(".swiper-promo-slider", {
    speed: 1000,
    parallax: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  });

  var screenshotsSlider = new Swiper(".swiper-screenshots", {
    effect: "coverflow",
    // loop: true,
    centeredSlides: true,
    grabCursor: true,
    keyboardControl: true,
    mousewheelControl: true,
    preventClicks: false,
    preventClicksPropagation: false,
    spaceBetween: 30,
    nextButton: ".swiper-button-next",
    prevButton: ".swiper-button-prev",
    slidesPerView: "auto",
    coverflowEffect: {
      rotate: 5,
      stretch: 0,
      depth: 100,
      modifier: 3,
      slideShadows: false
    }
  });

  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  var vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty("--vh", vh + "px");

  // We listen to the resize event
  window.addEventListener("resize", function() {
    // We execute the same script as before
    // let vh = window.innerHeight * 0.01;
    // document.documentElement.style.setProperty("--vh", `${vh}px`);
  });

  var menuButton = $("#izlude-m-btn");
  var menuSideNav = $("#payon-js-scroll");
  menuButton.on("click", function(e) {
    e.preventDefault();
    $("html").toggleClass("payon-open");

    $(".payon-cat-item").on("click", function(e) {
      e.preventDefault();
      $(".payon-cat-item").removeClass("active");
      $(this).addClass("active");
      var tabTar = $(this).attr("data-payon");
      // $(".payonTab").fadeOut();
      $("#" + tabTar)
        .show()
        .siblings()
        .hide();
    });
  });

  $(".new-catalog-item, .new-catalog-sub-item").on("click", function() {
    $("#modalCatalogMenu").modal("hide");
  });

  $(".call-mobile").click(function(e) {
    e.preventDefault();
    $(".call").toggleClass("is-active");
  });

  $(".contact-float-btn").on("click", function(e) {
    e.preventDefault();
    $(".contact-float-panel, .contact-float-overlay").toggleClass("active");
  });

  $(".contact-float-overlay").on("click", function(e) {
    $(".contact-float-panel, .contact-float-overlay").removeClass("active");
  });

  $(".close-customizer-btn").on("click", function(e) {
    e.preventDefault();
    $(".contact-float-panel,.contact-float-overlay").removeClass("active");
  });

  $("#SwitchMap").on("click", function(e) {
    if ($(this).prop("checked")) {
      // something when checked

      $("#collapseMap").collapse("show");
    } else {
      // something else when not
      $("#collapseMap").collapse("hide");
    }
  });

  var swiper = new Swiper(".swiper-single-place", {
    scrollbar: {
      el: ".swiper-scrollbar",
      hide: false
    },
    spaceBetween: 3,
    slidesPerView: "auto",
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3
    }
  });
  $(".form-select2").select2({
    placeholder: "Select a state"
  });
});



function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this,
      args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function autocompleteSearch(inp) {
  const searchQuickInput = document.querySelector(".search-quick-input");
  const searchQuickSuggestions = document.querySelector(".search-quick");

  const searchRecent = document.querySelector(".search-suggest");

  const urlMomo = "http://dev.momo.vn/__post/Search/Suggest";

  const githubAjax = "https://api.github.com/search/repositories";

  const searchQuickIcon = [
    {
      name: "Keyword",
      icon: "svg-search"
    },
    {
      name: "Article",
      icon: "svg-note"
    },
    {
      name: "QA",
      icon: "svg-money-question"
    },
    {
      name: "Guide",
      icon: "svg-momo-hand"
    },
    {
      name: "Promotion",
      icon: "svg-pig-cashback"
    },
    {
      name: "Service",
      icon: "svg-handshake"
    },
    {
      name: "MomoPage",
      icon: "svg-momo-logo"
    }
  ];

  var currentFocus;

  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = x.length - 1;
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("is-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("is-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.querySelectorAll(".search-quick-item");

    for (var i = 0; i < x.length; i++) {
      if (elmnt != searchQuickInput) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  function displayMatches(matchArray, txt) {
    const html = matchArray
      .map(item => {
        const regex = new RegExp(txt, "gi");
        const cont = item.Name.replace(
          regex,
          `<strong class="">${txt}</strong>`
        );
        const link = item.UrlRewrite;
        const type = item.Type;
        let icon = "";
        searchQuickIcon.forEach(function(e) {
          if (e.name == type) {
            icon = e.icon;
          }
        });

        return `
            <li role="presentation" class="search-quick-item">
                <a href="${link}" class="search-quick-link" target="_self" role="menuitem" tabindex="-1">

                    <svg class="svg-inline icon">
                        <use xlink:href="svg/svg.svg#${icon}"></use>
                    </svg>

                    <span data-purpose="label"> ${cont} </span>
                </a>
            </li>
        `;
      })
      .join("");
    searchQuickSuggestions.innerHTML = html;
  }

  var searchAjaxResult = debounce(function(e) {
    //   var txt = this.value.toLowerCase();
    var txt = this.value;

    if (!txt) {
      txt = "circle";
    }

    if (txt.length < 3 && txt) {
      closeAllLists();
      return;
    }

    currentFocus = -1;
    $.ajax({
      url: urlMomo,
      headers: {
        "X-Requested-With": "XMLHttpRequest",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: "POST",
      dataType: "json",
      cache: !1,
      data: {
        q: txt,
        c: "3" //Total shit
      },
      success: function(data) {
        // Data lấy đc về
        let dataGet = [];

        for (var property1 in data) {
          dataGet = dataGet.concat(data[property1].Items);
        }
        displayMatches(dataGet, txt);
      },
      error: function(error) {
        console.log(error);
      }
    });
  }, 300);

  document.addEventListener("click", function(e) {
    closeAllLists(e.target);
  });

  searchQuickInput.addEventListener("keydown", function(e) {
    var x = document.querySelector(".search-quick");
    if (x) x = x.getElementsByClassName("search-quick-item");

    if (e.keyCode == 40) {
      e.preventDefault();

      currentFocus++;
      addActive(x);
    } else if (e.keyCode == 38) {
      e.preventDefault();

      currentFocus--;
      addActive(x);
    } else if (e.keyCode == 13) {
      if (currentFocus > -1) {
        e.preventDefault();
        if (x) {
          location.href = x[currentFocus]
            .querySelector(".search-quick-link")
            .getAttribute("href");
        }
      }
    }
  });
  searchQuickInput.addEventListener("input", searchAjaxResult);
  searchQuickInput.addEventListener("focus", searchAjaxResult);
}
autocompleteSearch();
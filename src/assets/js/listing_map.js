(function(A) {
  if (!Array.prototype.forEach)
    A.forEach =
      A.forEach ||
      function(action, that) {
        for (var i = 0, l = this.length; i < l; i++)
          if (i in this) action.call(that, this[i], i, this);
      };
})(Array.prototype);

var mapObject,
  markers = [],
  markersData = {
    Marker: [
      {
        "StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "Bánh SHIT TUK TUK",
		"OwnerName": null,
		"Address": "34 CÔ BẮC",
		"CityId": 1,
		"DistrictId": 1,
		"WardId": 10275,
		"Latitude": "10.7657262",
		"Longitude": "106.6934443",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5151,
		"StoreAvatar": "images/dtt/ddt-1.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Siêu thị"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "Gà Ác Vĩnh Quý Quận 7",
		"OwnerName": null,
		"Address": "510Bis Nguyễn Thị Thập,",
		"CityId": 1,
		"DistrictId": 7,
		"WardId": 10356,
		"Latitude": "10.7383792",
		"Longitude": "106.7128678",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5150,
		"StoreAvatar": "images/dtt/ddt-2.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Cửa hàng"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "STU Coffee & Food",
		"OwnerName": null,
		"Address": "43 - 45 Nguyễn Chí Thanh,",
		"CityId": 1,
		"DistrictId": 5,
		"WardId": 10328,
		"Latitude": "10.7602846",
		"Longitude": "106.6697193",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5149,
		"StoreAvatar": "images/dtt/ddt-3.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Quán ăn đường phố"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "THE ALLEY TRẦN PHÚ",
		"OwnerName": null,
		"Address": "412-414-416 Trần Phú",
		"CityId": 1,
		"DistrictId": 5,
		"WardId": 10326,
		"Latitude": "10.7530168",
		"Longitude": "106.6659836",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5148,
		"StoreAvatar": "images/dtt/ddt-4.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Nhà hàng"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "THE ALLEY HỒNG BÀNG",
		"OwnerName": null,
		"Address": "493 Hồng Bàng",
		"CityId": 1,
		"DistrictId": 5,
		"WardId": 10333,
		"Latitude": "10.7536374",
		"Longitude": "106.6517442",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5147,
		"StoreAvatar": "images/dtt/ddt-1.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Cafe/Dessert"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "Cafe Sakura",
		"OwnerName": null,
		"Address": "179 An Dương Vương",
		"CityId": 1,
		"DistrictId": 5,
		"WardId": 10327,
		"Latitude": "10.7565822",
		"Longitude": "106.666626",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5146,
		"StoreAvatar": "images/dtt/ddt-2.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Giải trí"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "Cháo Trắng Cháo Đậu HP 2",
		"OwnerName": null,
		"Address": "16 Hoàng Minh Đạo",
		"CityId": 1,
		"DistrictId": 8,
		"WardId": 10363,
		"Latitude": "10.7372672",
		"Longitude": "106.6605004",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5145,
		"StoreAvatar": "images/dtt/ddt-3.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Nhà hàng"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "PHÚC LỢI HỒNG HÀ",
		"OwnerName": null,
		"Address": "154 Hồng Hà",
		"CityId": 1,
		"DistrictId": 16,
		"WardId": 10496,
		"Latitude": "10.8085272",
		"Longitude": "106.6730571",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5144,
		"StoreAvatar": "images/dtt/ddt-4.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Mua sắm"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "PHÚC LỢI LÂM VĂN BỀN",
		"OwnerName": null,
		"Address": "10 Lâm Văn Bền",
		"CityId": 1,
		"DistrictId": 7,
		"WardId": 10354,
		"Latitude": "10.7502264",
		"Longitude": "106.7136919",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5143,
		"StoreAvatar": "images/dtt/ddt-1.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Mua sắm"
	  },
	  {
		"StatusName": null,
		"WardName": null,
		"DistrictName": null,
		"CityName": null,
		"StoreName": "LẨU KIKI",
		"OwnerName": null,
		"Address": "175 Nguyễn Đức Cảnh",
		"CityId": 1,
		"DistrictId": 7,
		"WardId": 10357,
		"Latitude": "10.7215086",
		"Longitude": "106.7098383",
		"Type": 2,
		"Status": null,
		"CreatedOn": null,
		"CreatedBy": null,
		"UpdatedOn": null,
		"UpdatedBy": null,
		"Id": 5142,
		"StoreAvatar": "images/dtt/ddt-2.jpg",
		"StoreImages": "images/dtt/ddt-avatar1.jpg",
		"Phone" : "02854161106",
		"TimeOpen" : "00:00 - 23:59",
		"StoreStatus": "Đang mở cửa",
		"StoreType": "Mua sắm"
	  }
    ]
  };

var mapOptions = {
  zoom: 12,
  center: new google.maps.LatLng(10.7215086, 106.7098383),
  mapTypeId: google.maps.MapTypeId.ROADMAP,

  mapTypeControl: false,
  mapTypeControlOptions: {
    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
    position: google.maps.ControlPosition.LEFT_CENTER
  },
  panControl: false,
  panControlOptions: {
    position: google.maps.ControlPosition.TOP_RIGHT
  },
  zoomControl: true,
  zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_BOTTOM
  },
  scrollwheel: false,
  scaleControl: false,
  scaleControlOptions: {
    position: google.maps.ControlPosition.TOP_LEFT
  },
  streetViewControl: true,
  streetViewControlOptions: {
    position: google.maps.ControlPosition.LEFT_TOP
  },
  styles: [
    {
      featureType: "administrative.country",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.province",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.locality",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.neighborhood",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.land_parcel",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "landscape.man_made",
      elementType: "all",
      stylers: [
        {
          visibility: "simplified"
        }
      ]
    },
    {
      featureType: "landscape.natural.landcover",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "landscape.natural.terrain",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.government",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.medical",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.place_of_worship",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.school",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.sports_complex",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.highway.controlled_access",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.arterial",
      elementType: "all",
      stylers: [
        {
          visibility: "simplified"
        }
      ]
    },
    {
      featureType: "road.local",
      elementType: "all",
      stylers: [
        {
          visibility: "simplified"
        }
      ]
    },
    {
      featureType: "transit.line",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "transit.station.airport",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "transit.station.bus",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "transit.station.rail",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }
  ]
};


if(document.getElementById("map_right_listing")){

	var marker;

	mapObject = new google.maps.Map(
		document.getElementById("map_right_listing"),
		mapOptions
	);
	for (var key in markersData)
		markersData[key].forEach(function(item) {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(
					item.Latitude,
					item.Longitude
				),
				map: mapObject,
				icon: {
					url: "images/dtt/pins/" + key + ".svg",
					scaledSize: new google.maps.Size(40, 40)
				}
			});

			if ("undefined" === typeof markers[key]) markers[key] = [];
			markers[key].push(marker);
			google.maps.event.addListener(marker, "click", function() {
				closeInfoBox();
				getInfoBox(item).open(mapObject, this);
				mapObject.setCenter(
					new google.maps.LatLng(item.Latitude, item.Longitude)
				);
			});
		});

	new MarkerClusterer(mapObject, markers[key]);


}


function hideAllMarkers() {
  for (var key in markers)
    markers[key].forEach(function(marker) {
      marker.setMap(null);
    });
}

function closeInfoBox() {
  $("div.infoBox").remove();
}

function getInfoBox(item) {
  return new InfoBox({
    content:
      '<div class="marker_info" id="marker_info">' +
      '<img src="' +
      item.StoreImages +
      '" alt=""/>' +
      "<span>" +
      '<span class="infobox_rate">' + item.StoreStatus + " - " +
      item.TimeOpen +
      "</span>" +
      "<em>" +
      item.StoreType +
      "</em>" +
      "<h3>" +
      item.StoreName +
      "</h3>" +
      '<form action="http://maps.google.com/maps" method="get" target="_blank"><input name="saddr" value="" type="hidden"><input type="hidden" name="daddr" value="' +
      item.Latitude +
      "," +
      item.Longitude +
      '"><button type="submit" value="Get directions" class="btn_infobox_get_directions"> <img src="images/dtt/compass.svg" class="infobox-icon"/> Tới địa điểm này</button></form>' +
      '<a href="tel://' +
      item.Phone +
      '" class="btn_infobox_phone"> <img src="images/dtt/call-answer.svg" class="infobox-icon"/>' +
      item.Phone +
      "</a>" +
      "</span>" +
      "</div>",
    disableAutoPan: false,
    maxWidth: 0,
	pixelOffset: new google.maps.Size(-120, -50),
    closeBoxMargin: "",
    closeBoxURL: "images/dtt/close_infobox.png",
    isHidden: false,
    alignBottom: true,
    pane: "floatPane",
    enableEventPropagation: true
  });
}
function onMapClick(location_type, key) {

  google.maps.event.trigger(markers[location_type][key], "click");
// mapObject.setCenter(
// 	new google.maps.LatLng(markersData.Marker[key].Latitude, markersData.Marker[key].Longitude)
// );
	$("html, body").animate({ scrollTop: 0 }, "slow");

}




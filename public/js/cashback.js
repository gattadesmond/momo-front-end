/*
Template Name: Osahan Eat - Online Food Ordering Website HTML Template
Author: Askbootstrap
Author URI: https://themeforest.net/user/askbootstrap
Version: 1.0
*/
(function ($) {
    "use strict"; // Start of use strict

    // ===========Select2============
    $('select').select2();




    /* 


        const objowlcarousel = $('.owl-carousel-category');
        if (objowlcarousel.length > 0) {
            objowlcarousel.owlCarousel({
                responsive: {
                    0: {
                        items: 3,
                    },
                    600: {
                        items: 4,
                    },
                    1000: {
                        items: 6,
                    },
                    1200: {
                        items: 8,
                    },
                },
                loop: true,
                lazyLoad: true,
                autoplay: true,
                dots: false,
                autoplaySpeed: 1000,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                nav: true,
                navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            });
        }

        var fiveobjowlcarousel = $(".owl-carousel-four");
        if (fiveobjowlcarousel.length > 0) {
            fiveobjowlcarousel.owlCarousel({
                responsive: {
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 2,
                    },
                    1000: {
                        items: 4,
                    },
                    1200: {
                        items: 4,
                    },
                },

                lazyLoad: true,
                pagination: false,
                loop: true,
                dots: false,
                autoPlay: 2000,
                nav: true,
                stopOnHover: true,
                navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"]
            });
        }

        var fiveobjowlcarousel = $(".owl-carousel-five");
        if (fiveobjowlcarousel.length > 0) {
            fiveobjowlcarousel.owlCarousel({
                responsive: {
                    0: {
                        items: 2,
                    },
                    600: {
                        items: 3,
                    },
                    1000: {
                        items: 4,
                    },
                    1200: {
                        items: 5,
                    },
                },
                lazyLoad: true,
                pagination: false,
                loop: true,
                dots: false,
                autoPlay: 2000,
                nav: true,
                stopOnHover: true,
                navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"]
            });
        }
        
        */




    var cashbackAd = new Swiper(".cashback-ad", {
        spaceBetween: 0,
        slidesPerView: 1,
        lazy: {
            loadPrevNext: true
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        },
        pagination: {
            el: ".swiper-pagination"
        },
        on: {
            slideChangeTransitionEnd: function slideChangeTransitionEnd() {
                var num = cashbackAd.activeIndex;
                $(".cashback-cat-nav .is-active").removeClass("is-active");
                $(".cashback-cat-nav .swiper-slide")
                    .eq(num)
                    .addClass("is-active");

                if (this.previousIndex < this.activeIndex) {
                    cashbackCat.slideTo(this.activeIndex - 3);
                } else {
                    cashbackCat.slideTo(this.activeIndex - 1);
                }
            }
        }
    });

    var cashbackCat = new Swiper(".cashback-cat-nav", {
        spaceBetween: 0,
        slidesPerView: "auto"
    });

    $(".cashback-cat-nav .swiper-slide").each(function (index, element) {
        var num = index;
        $(element).on("click", function (e) {
            e.preventDefault();
            cashbackAd.slideTo(index);
        });
    });














    var swiperProduct = new Swiper(".cashback-new", {
        slidesPerView: 'auto',

        spaceBetween: 20,

        // Enable lazy loading
        lazy: {
            loadPrevNext: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });


    var swiperProduct = new Swiper(".swiper-cashback", {
        slidesPerView: 'auto',
        spaceBetween: 16,
        // Enable lazy loading
        lazy: {
            loadPrevNext: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

    var swiperProduct = new Swiper(".cashback-cate", {
        slidesPerView: 8,
        slidesPerGroup: 1,
        spaceBetween: 0,
        breakpoints: {
            1200: {
                slidesPerView: 8
            },
            1000: {
                slidesPerView: 6
            },
            600: {
                slidesPerView: 4
            },
            320: {
                slidesPerView: 3
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });



    $("#juno-menu-aim").menuAim({
        activate: function activate(row) {
            $(row)
                .children()
                .addClass("is-active");
        },
        deactivate: function deactivate(row) {
            $(row)
                .children()
                .removeClass("is-active");
        },
        exitMenu: function exitMenu() {
            $(".cd-dropdown-content")
                .find(".is-active")
                .removeClass("is-active");
            return true;
        },
        submenuDirection: "right"
    });



    var cashbackTime = $(".cashback-countdown");
    cashbackTime.length > 0 &&
        cashbackTime.each(function () {
            var e = $(this),
                i = e.attr("data-date");
            e.countdown(i).on("update.countdown", function (e) {
                $(this).html(
                    e.strftime('%D ngày %H:%M:%S')
                );
            });
        });
    /* 
        const mainslider = $('.homepage-ad');
        if (mainslider.length > 0) {
            mainslider.owlCarousel({
                responsive: {
                    0: {
                        items: 2,
                    },
                    764: {
                        items: 2,
                    },
                    765: {
                        items: 1,
                    },
                    1200: {
                        items: 1,
                    },
                },
                lazyLoad: true,
                loop: true,
                autoplay: true,
                autoplaySpeed: 1000,
                dots: false,
                autoplayTimeout: 2000,
                nav: true,
                navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                autoplayHoverPause: true,
            });
        }
     */
    // Tooltip
    $('[data-toggle="tooltip"]').tooltip()




})(jQuery); // End of use strict